import discord
from discord.ext import commands
import asyncio
import os
import util    
import twitter
import login
import checks
import settings


class Twitter:
    """Twitter cog that fetches tweets"""

    def __init__(self, bot):
        self.bot = bot
        self.bound_channels_file = "data" + os.sep + "bound_channels.json"
        self.last_tweet_ids_file = "data" + os.sep + "last_tweet_ids.json"
        self.bound_channels = util.load_json(self.bound_channels_file)
        self.last_tweet_ids = util.load_json(self.last_tweet_ids_file)
        self.update_rate = 180        
        self.twitter_api = twitter.Api(consumer_key = login.twitter_key ,consumer_secret = login.twitter_secret, 
                                       access_token_key = login.twitter_access_key, access_token_secret = login.twitter_access_secret)


    @commands.command(pass_context = True)
    @checks.is_authorised()
    async def bind(self, ctx, channel : discord.Channel):
        '''Binds the bot to a text channel.
        
        Usage example:
        !bind #general'''
        server = ctx.message.server
        author = ctx.message.author

        if server.id in self.bound_channels:
            if self.bound_channels[server.id] == channel.id:
                await self.bot.say('Already bound to channel <#{}> on "{}" '.format(channel.id, server.name), 
                                   delete_after = settings.delete_message_time)
            else:
                await self.bot.say('The bot is already bound to channel <#{}> on "{}", only one channel per server allowed.'
                                   .format(self.bot.get_channel(self.bound_channels[server.id]).id, server.name), 
                                   delete_after = settings.delete_message_time)
        else:
            print('{} bound the bot to channel #{} on "{}" '.format(author.name, channel.name, server.name))
            await self._bind_to_channel(channel)


    @commands.command(pass_context = True)
    @checks.is_authorised()
    async def unbind(self, ctx, channel : discord.Channel):
        '''Unbinds the bot from a text channel.
        
        Usage example:
        !unbind #general'''
        server = ctx.message.server
        author = ctx.message.author
        
        # check if channel is bound
        if server.id in self.bound_channels and self.bound_channels[server.id] == channel.id:

            del self.bound_channels[channel.server.id]
            # write updated dict to file
            util.save_json(self.bound_channels_file, self.bound_channels)

            print('{} unbound the bot from channel #{} on "{}" '.format(author.name, channel.name, server.name))
            await self.bot.say('Successfully unbound from channel <#{}> on "{}" '.format(channel.id, server.name), delete_after = settings.delete_message_time)
        else:
            await self.bot.say("Twibo isn't bound to <#{}>".format(channel.id), delete_after = settings.delete_message_time)


    @commands.command(pass_context = True)
    @checks.is_authorised()
    async def follow(self, ctx, twitter_user:str, tweet_count:int = 3):
        '''Adds a twitter user to the followed user list on this server.
        
        Usage example:
        !follow talonro'''
        server = ctx.message.server
        author = ctx.message.author        

        if not self._valid_twitter_user(twitter_user):
            await self.bot.say("{} is not a valid twitter user.".format(twitter_user), delete_after = settings.delete_message_time)
            return

        if server.id not in self.last_tweet_ids:
            self.last_tweet_ids[server.id] = {}

        if twitter_user in self.last_tweet_ids[server.id]:
            await self.bot.say("Twibo is already following this user on this server.", delete_after = settings.delete_message_time)
        else:
            self.last_tweet_ids[server.id][twitter_user] = 0
            print('{} added {} to the followed user list on "{}" '.format(author.name, twitter_user, server.name))

            # print tweets of this user
            tweet_count = util.clamp(tweet_count, 0, 20)
            bound_channel = self.bound_channels[server.id]
            await self._fetch_and_post(bound_channel, twitter_user, 0, max_count = tweet_count)
            await self.bot.say("Successfully added the twitter user to the list.", delete_after = settings.delete_message_time)


    @commands.command(pass_context = True)
    @checks.is_authorised()
    async def unfollow(self, ctx, twitter_user:str):
        '''Removes a twitter user from the followed user list on this server.
        
        Usage example:
        !unfollow talonro'''
        server = ctx.message.server
        author = ctx.message.author        

        if server.id not in self.last_tweet_ids \
        or twitter_user not in self.last_tweet_ids[server.id]:
            await self.bot.say("Twibo is not following this user on this server.", delete_after = settings.delete_message_time)
        else:
            del self.last_tweet_ids[server.id][twitter_user]
            util.save_json(self.last_tweet_ids_file, self.last_tweet_ids)
            print('{} removed {} from the followed user list on "{}" '.format(author.name, twitter_user, server.name))
            await self.bot.say("Successfully removed the twitter user from the list.", delete_after = settings.delete_message_time)


    # displays the followed twitter user for the current server
    @commands.command(pass_context = True)
    async def following(self, ctx):
        '''Displays the followed twitter users for this server.
        
        Usage example:
        !following'''
        server = ctx.message.server

        text = ""
        if server.id in self.last_tweet_ids:
            text = self._get_followed_accounts(server)

        if text != "":
            await self.bot.say(text) # dont delete message
        else:
            await self.bot.say("Twibo isn't following any twitter users on this server.", delete_after = settings.delete_message_time)
        
        
    # displays the followed twitter user for the current server
    @commands.command(pass_context = True)
    @checks.is_owner()
    async def followingall(self, ctx):
        '''Displays the followed twitter users for all servers.
        
        Usage example:
        !followingall''' 
        author = ctx.message.author 
        
        text = ""
        for server in self.bot.servers:
            server = self.bot.get_server(server.id)
            text += self._get_followed_accounts(server, ctx.message.server) + "\n\n"

        if text != "":
            await self.bot.say(text, delete_after = settings.delete_message_time)
        else:
            await self.bot.say("Twibo isn't following any twitter users at all.", delete_after = settings.delete_message_time)


    @commands.command(pass_context = True)
    @checks.is_owner()
    async def setrate(self, ctx, seconds:int):
        '''Sets the time between fetching new tweets, the minimum rate is 30 seconds.
        
        Usage example:
        !bind setrate 60'''
        self.update_rate = max(30, seconds)
        await self.bot.say("The update rate was set to {}".format(self.update_rate), delete_after = settings.delete_message_time)


    # returns the list of followed twitter accounts as a formatted text
    def _get_followed_accounts(self, server, from_server = None):
        text = ""

        if server.id not in self.bound_channels:
            text += "**{}:** not bound yet\n".format(server.name)
        else:
            channel = self.bot.get_channel(self.bound_channels[server.id])
            if channel == None:
                # channel doesnt exist anymore
                print("Removed deleted channel on {}".format(self.bot.get_server(server.id).name))
                del self.bound_channels[server.id]
                util.save_json(self.bound_channels_file, self.bound_channels)
                text += "**{}:** was bound to deleted channel, removed binding\n".format(server.name)
            elif server is from_server:
                text += "**{}:** bound to <#{}>\n".format(server.name, channel.id)
            else:
                text += "**{}:** bound to #{}\n".format(server.name, channel.name)

        text += "__Following:__\n"
        if server.id not in self.last_tweet_ids:
            text += "- none\n"
        else:
            for twitter_user, last_tweet_id in self.last_tweet_ids[server.id].items():
                text += "- user name: **@{}** display name: **{}**\n".format(twitter_user, self._get_twitter_display_name(twitter_user))
        return text


    # prints the bound channels to the terminal
    def _print_channels_to_terminal(self):
        # check if dictionary is empty
        if not self.bound_channels:
            print("Bot isn't bound to any channels!")
            return
        for server, channel in self.bound_channels.items():
            print("Bound channel #{} on server {}.".format(self.bot.get_channel(channel), self.bot.get_server(server)))


    # binds the bot to the channel
    async def _bind_to_channel(self, channel:discord.Channel):
        self.bound_channels[channel.server.id] = channel.id
        util.save_json(self.bound_channels_file, self.bound_channels)
        await self.bot.say('Successfully bound to channel <#{}> on "{}" '.format(channel.id, channel.server.name), delete_after = settings.delete_message_time)


    # fetches latest tweets and posts them to the channel
    async def _fetch_and_post(self, discord_channel:int, twitter_user:str, last_tweet_id:int, max_count:int = None):
        tweets = self.twitter_api.GetUserTimeline(screen_name = twitter_user, since_id = last_tweet_id, count = max_count)
        channel = self.bot.get_channel(str(discord_channel))

        last_id = self.last_tweet_ids[channel.server.id][twitter_user]

        for tweet in map(lambda x: x.AsDict(), reversed(tweets)):
            tweet_id = tweet["id_str"]
            self.last_tweet_ids[channel.server.id][twitter_user] = tweet_id
            if max_count == 0:
                continue

            screen_name = tweet["user"]["name"]
            date = tweet["created_at"].split("+")[0]
            tweet_text = tweet["text"]

            media = ""
            if "media" in tweet.keys():
                for media_link in tweet["media"]:
                    media += media_link["media_url"] + "\n"

            text = "__**{0} {1}:**__\n{2}\n".format(screen_name, date,tweet_text)#, media)
            await self.bot.send_message(channel, text)

        if last_id != self.last_tweet_ids[channel.server.id][twitter_user]:
            util.save_json(self.last_tweet_ids_file, self.last_tweet_ids)


    # fetches all tweets and posts them to all bound channels
    async def _fetch_all_tweets(self):
        for server_id, twitter_users in self.last_tweet_ids.items():
            if server_id not in self.bound_channels:
                # not bound to this server
                break
            channel = self.bound_channels[server_id]
            if self.bot.get_channel(channel) == None:
                # channel doesnt exist anymore
                print("Removed deleted channel on {}".format(server_id))
                del self.bound_channels[server_id]
                util.save_json(self.bound_channels_file, self.bound_channels)
                continue

            for twitter_user, last_tweet_id in self.last_tweet_ids[server_id].items():
                await self._fetch_and_post(channel, twitter_user, last_tweet_id)


    # checks if the name is a valid user
    def _valid_twitter_user(self, name):
        try:
            user = self.twitter_api.GetUser(screen_name = name)
            return True
        except twitter.error.TwitterError as ex:
            return False    


    # returns the display name of the twitter user
    def _get_twitter_display_name(self, name):
        try:
            displayname = self.twitter_api.GetUser(screen_name = name).name
            return displayname
        except twitter.error.TwitterError as ex:
            return "TwitterError"


    # automaticly gets called
    async def on_ready(self):
        print("Twitter cog is ready!")
        self._print_channels_to_terminal()
        self.bot.loop.create_task(self.background_task())


    # the background task that looks for new tweets
    async def background_task(self):
        await self.bot.wait_until_ready()
        while not self.bot.is_closed:
            await self._fetch_all_tweets()
            await asyncio.sleep(self.update_rate)


def setup(bot):
    bot.add_cog(Twitter(bot))