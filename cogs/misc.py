import discord
from discord.ext import commands
import settings

class Misc:
    def __init__(self, bot):
        self.bot = bot
        self.link = "https://bitbucket.org/ArminHe/twibo/src"


    # displays the followed twitter user for the current server
    @commands.command()
    async def code(self):
        '''Returns the link to the Twibo source code.
        
        Usage example:
        !code'''
        await self.bot.say("You can find the Twibo source code here: {}".format(self.link), delete_after = settings.delete_message_time)


    @commands.command(pass_context = True)
    async def setnick(self, ctx, nickname:str = None):
        '''Sets the nickname for the bot on this server.
        
        Usage example:
        !bind setnick robert'''
        await self.bot.change_nickname(ctx.message.server.me, nickname)
        if nickname is not None:
            await self.bot.say("The nickname was set to {}".format(nickname), delete_after = settings.delete_message_time)
        else:
            await self.bot.say("The nickname was reset", delete_after = settings.delete_message_time)


    @commands.group(pass_context = True, invoke_without_command = True)
    async def info(self, ctx, *, member : discord.Member = None):
        '''Displays information about a user, displays your infos if you didnt specify an user.
        
        Usage example:
        !info, !info @user or !info server'''
        channel = ctx.message.channel
        if member is None:
            member = ctx.message.author

        roles_strings = [x.name.replace("@", "@\u200b") for x in member.roles]

        text = "```"
        text += "Name: {}\n".format(member.name)
        text += "ID: {}\n".format(member.id)

        if member.nick is not None:
            text += "Nickname: {}\n".format(member.nick)

        text += "Bot: {}\n".format(member.bot)
        text += "Created: {}\n".format(member.created_at.strftime("%Y-%m-%d %H:%M:%S"))
        text += "Joined: {}\n".format(member.joined_at.strftime("%Y-%m-%d %H:%M:%S"))
        text += "Roles: {}\n".format( ", ".join(roles_strings))

        if member.game is not None:
            text+= "Playing: {}\n".format(member.game.name)
        
        text += "```"
        text += member.avatar_url

        await self.bot.say(text, delete_after = settings.delete_message_time)


    @info.command(name = 'server', pass_context = True)
    async def server_info(self, ctx):
        server = ctx.message.server

        # lists of information
        text_channel_strings = ["#{} {}".format(x.name, x.id) for x in server.channels if x.type == discord.ChannelType.text]
        voice_channel_strings = ["{} {}".format(x.name, x.id) for x in server.channels if x.type == discord.ChannelType.voice]
        member_strings = [x.name for x in server.members]
        role_strings = [x.name.replace("@", "@\u200b") for x in server.roles]

        text = "```"
        text += "Name: {}\n".format(server.name)
        text += "ID: {}\n".format(server.id)
        text += "Owner: {}\n".format(server.owner.name)
        text += "Created: {}\n".format(server.created_at.strftime("%Y-%m-%d %H:%M:%S"))
        text += "Region: {}\n".format(server.region.name.title())
        text += "Text Channels: {}\n".format(", ".join(text_channel_strings))
        text += "Voice Channels: {} \n".format(", ".join(voice_channel_strings))
        text += "Default Channel: #{}\n".format(server.default_channel.name)

        if server.afk_channel is not None:
            text += "AFK Channel: {}\n".format(server.afk_channel)
            text += "AFK Timeout: {} seconds\n".format(server.afk_timeout)

        text += "Member Count: {}\n".format(server.member_count)
        text += "Members: {}\n".format( ", ".join(member_strings))
        text += "Roles: {}\n".format( ", ".join(role_strings))
        text += "```"
        text += server.icon_url

        await self.bot.say(text, delete_after = settings.delete_message_time)


    # automaticly gets called
    async def on_ready(self):
        print("Misc cog is ready!")


def setup(bot):
    bot.add_cog(Misc(bot))
