from discord.ext import commands
import settings


def is_owner():
    return commands.check(lambda ctx: _is_owner_check(ctx))


def is_authorised():
    return commands.check(lambda ctx: _is_authorised_check(ctx))


def _is_owner_check(ctx):
    return ctx.message.author.id == settings.owner


def _is_authorised_check(ctx):
    if _is_owner_check(ctx):
        return True
    elif ctx.message.author.id == ctx.message.server.owner_id:
        return True
    else:
        return False