import os
import json
import settings
import discord

def is_int(string):
    try:
        int(string)
        return True
    except ValueError:
        return False


def is_channel(string):
    return string.startswith("<#") and string.endswith(">")

def clamp(n, smallest, largest): 
    return max(smallest, min(n, largest))


def load_json(path):
    dir = os.path.dirname(path)
    if not os.path.exists(dir):
        print("Directory created! " + dir)
        os.mkdir(dir)

    if not os.path.isfile(path):
        save_json(path, {})

    if os.path.isfile(path):
        print("Loading from file: " + path)
        with open(path, encoding = "utf-8", mode = "r") as fp:
            return json.load(fp)


def save_json(path, data):
    with open(path, encoding = "utf-8", mode = "w") as fp:
        # print("File updated: " + path)
        json.dump(data, fp, sort_keys = True, indent = 4)