from discord.ext import commands
import settings
import login
from cogs import twitter, misc
import traceback


bot = commands.Bot(command_prefix = settings.prefix, description = settings.description)

@bot.event
async def on_ready():
    print("Logged in as")
    print(bot.user.name)
    print(bot.user.id)
    print("------")


@bot.event
async def on_command_error(exception, ctx):

    # print to discord
    if isinstance(exception, (commands.MissingRequiredArgument, commands.BadArgument)):
        exception_name = type(exception).__name__
        command_name_text = ""
        help = ""
        help_text = None

        if ctx.command is not None:
            command_name_text = " in command {}".format(ctx.command)

        if ctx.invoked_subcommand:
            help_text = bot.formatter.format_help_for(ctx, ctx.invoked_subcommand)
        else:
            help_text = bot.formatter.format_help_for(ctx, ctx.command)

        for line in help_text:
            help += line

        text = "**{} error{}:**\n{}\n\n{}".format(exception_name, command_name_text, exception, help)
        await bot.send_message(ctx.message.channel, text)
    elif isinstance(exception, commands.CheckFailure):
        print("User {} tried to use {} on server {}.".format(ctx.message.author, ctx.command.name, ctx.message.server.name))
        await bot.send_message(ctx.message.channel, "You don't have permission to use the {} command.".format(ctx.command.name))
        return # dont print to the consolde
    elif isinstance(exception, commands.CommandNotFound):
        return

    # print to terminal too
    print('Ignoring exception in command {}'.format(ctx.command))
    traceback.print_exception(type(exception), exception, exception.__traceback__)


# add cogs
bot.add_cog(twitter.Twitter(bot))
bot.add_cog(misc.Misc(bot))

bot.run(login.discord_token)